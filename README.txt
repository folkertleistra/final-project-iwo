How to use the bash script: extract_tweets.sh
author: Folkert Leistra s3469573
This is a script that was used for our research for the course Introduction to research methods.
----------------------------------------------------
extract_tweets.sh is a bash script that looks for political party mentions in tweets. In this case: PVV, VVD, and PvdA.
The file that the script looks in is located at ../../net/corpora/twitter2/Tweets
Depending on location from which you run the script, you might have to change the path that the script takes in order to extract the tweets from the files.

The script cleans the tweets by using the following tools: tweet2tab and twclean -  located at: ../../net/corpora/twitter2/Tools

The script removes any @mentions, RT' s and URLS. 
