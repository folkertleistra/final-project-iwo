#!/bin/bash
# File name: twitter.sh
# $chmod +x file_name.sh
# This program extracts tweets from the RUG Database given a specific month (feb,mar,aprl). The script cleans the tweets by removing RT' s, @mentions and URLs and writes the output to a text file.
# The program searches in the files for political party names (VVD,PVV and PvdA). The program counts for each date how many tweets are in the file and how many tweets mention a specific party and how many tweets don' t mention a party.
# This program will only work from the home directory on a karora server/LWP PC at the RUG. If needed chance the path to the files with tweets (../../../net/corpora/twitter2/etc)`
# The bash script uses tools which are only available at the RUG/karora. Make sure you can acces them.

zcat ../../../net/corpora/twitter2/Tweets/2017/03/20170314* | ../../../net/corpora/twitter2/tools/./tweet2tab -i text | grep -v "^RT" | ../../../net/corpora/twitter2/tools/./twclean ua > tweets_mar14.txt
#This command takes the path to the tweets from march 14 2017. You might have to modify the path depending on the location you run the script from.
#The command extracts all tweets from march 14 2017 except for those starting with RT. The tweets are cleaned via the tool twclean. twclean ua removes all @mentions and URL' s. The tweets are written to the file: tweets_mar14.txt


zcat ../../../net/corpora/twitter2/Tweets/2017/03/20170325* | ../../../net/corpora/twitter2/tools/./tweet2tab -i text | grep -v "^RT" | ../../../net/corpora/twitter2/tools/./twclean ua > tweets_mar25.txt
#This command takes the path to the tweets from march 25 2017. You might have to modify the path depending on the location you run the script from.
#The command extract all tweets from march 25 2017, except for those starting with RT. The tweets are cleaned via the tool: twclea. twclean ua removes all @mentions and URL' s. The tweets are written to the file: tweets_mar25.txt


VVD_MAR14=$(cat tweets_mar14.txt | grep -wi '\<vvd\>' | wc -l)
PVDA_MAR14=$(cat tweets_mar14.txt | grep -wi '\<pvda\>' | wc -l)
PVV_MAR14=$(cat tweets_mar14.txt | grep -wi '\<pvv\>' | wc -l)
TOTAL_MAR14=$(cat tweets_mar14.txt | wc -l)
echo "--ELECTION DATE MARCH 15 2017--"
echo --------------------------------
#prints the results for march 14th 2017
echo "DATE:	MARCH 14 - 2017"
echo "TWEETS:	$TOTAL_MAR14"
echo "PARTY	FREQUENCY"
echo "VVD	$VVD_MAR14"
echo "PvdA	$PVDA_MAR14"
echo "PVV	$PVV_MAR14"
echo ---------------------------
echo "Tweets that don't mention VVD: $(($TOTAL_MAR14 - $VVD_MAR14))"
echo "Tweets that don't mention PvdA: $(($TOTAL_MAR14 - $PVDA_MAR14))"
echo "Tweets that don't mention PVV: $(($TOTAL_MAR14 - $PVV_MAR14))"
echo ----------------------------
#Results from march 25th 2017
VVD_MAR25=$(cat tweets_mar25.txt | grep -wi '\<vvd\>' | uniq | wc -l)
PVDA_MAR25=$(cat tweets_mar25.txt | grep -wi '\<pvda\>' | uniq | wc -l)
PVV_MAR25=$(cat tweets_mar25.txt | grep -wi '\<pvv\>' | uniq | wc -l)
TOTAL_MAR25=$(cat tweets_mar25.txt | wc -l)

echo "DATE:	MARCH 25 - 2017"
echo "TWEETS:	$TOTAL_MAR25"
echo "PARTY	FREQUENCY"
echo "VVD	$VVD_MAR25"
echo "PvdA	$PVDA_MAR25"
echo "PVV	$PVV_MAR25"
echo ---------------------------
echo "Tweets that don't mention VVD: $(($TOTAL_MAR25 - $VVD_MAR25))"
echo "Tweets that don't mention PvdA: $(($TOTAL_MAR25 - $PVDA_MAR25))"
echo "Tweets that don't mention PVV: $(($TOTAL_MAR25 - $PVV_MAR25))"
echo ---------------------------
